#include "cvars.qh"
#include "sounds.qh"

#include "announcer.qc"
#include "commands/all.inc"
#include "custom_playermodels.qc"
#include "effects.inc"
#include "hook.qc"
#include "location.qc"
#include "projectiles.qh"
#include "monsters/all.inc"
#include "nocross.qc"
#include "mutators/all.inc"
#include "notifications.qh"
#include "player_crush.qc"
#include "polytrails.qh"
#include "radio/module.inc"
#include "special.qc"
#include "weapons/all.inc"
#include "weapons.qc"

#define IMPLEMENTATION
#include "monsters/all.inc"
#undef IMPLEMENTATION

#include "killme/all.inc"
