#include "cvars.qh"

#ifndef WEP_NO_CHAINSAW
#include "chainsaw.qc"
#endif

#ifndef WEP_NO_UZI
#include "uzi.qc"
#endif

#ifndef WEP_NO_FLAK
#include "flak.qc"
#endif

#ifndef WEP_NO_LIGHTSABRE
#include "lightsabre.qc"
#endif

#ifndef WEP_NO_RPG
#include "rpg7.qc"
#endif

#ifndef WEP_NO_ASSAULTRIFLE
#include "assaultrifle.qc"
#endif

#ifndef WEP_NO_NEXGUN
#include "nexgun.qc"
#endif

#ifndef WEP_NO_RIFLEGUN
#include "riflegun.qc"
#endif
