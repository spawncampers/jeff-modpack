#include "riflegun.qh"

#ifdef SVQC

void W_Riflegun_FireBullet(Weapon thiswep, .entity weaponentity, float pSpread, float pDamage, float pHeadshotDamage, float pForce, float pSolidPenetration, float pAmmo, int deathtype, float pTracer, float pShots, Sound pSound, entity actor)
{
	float i;

	W_DecreaseAmmo(thiswep, actor, pAmmo, weaponentity);

	W_SetupShot(actor, weaponentity, true, 2, pSound, CH_WEAPON_A, pDamage * pShots, deathtype);

	W_MuzzleFlash(thiswep, actor, weaponentity, w_shotorg, w_shotdir * 2);

	if(PHYS_INPUT_BUTTON_ZOOM(actor) | PHYS_INPUT_BUTTON_ZOOMSCRIPT(actor)) // if zoomed, shoot from the eye
	{
		w_shotdir = v_forward;
		w_shotorg = actor.origin + actor.view_ofs + ((w_shotorg - actor.origin - actor.view_ofs) * v_forward) * v_forward;
	}

	for(i = 0; i < pShots; ++i)
		fireBullet(actor, weaponentity, w_shotorg, w_shotdir, pSpread, pSolidPenetration, pDamage, pHeadshotDamage, pForce, deathtype, (pTracer ? EFFECT_RIFLE : EFFECT_RIFLE_WEAK));

	if(autocvar_g_casings >= 2)
	{
		makevectors(actor.v_angle); // for some reason, this is lost
		SpawnCasing(((random() * 50 + 50) * v_right) - (v_forward * (random() * 25 + 25)) - ((random() * 5 - 70) * v_up), vectoangles(v_forward), 3, actor, weaponentity);
	}
}

void W_Riflegun_Attack(Weapon thiswep, entity actor, .entity weaponentity)
{
	W_Riflegun_FireBullet(thiswep, weaponentity, WEP_CVAR_PRI(WEP_RIFLEGUN, spread), WEP_CVAR_PRI(WEP_RIFLEGUN, damage), WEP_CVAR_PRI(WEP_RIFLEGUN, headshot_multiplier), WEP_CVAR_PRI(WEP_RIFLEGUN, force), WEP_CVAR_PRI(WEP_RIFLEGUN, solidpenetration), WEP_CVAR_PRI(WEP_RIFLEGUN, ammo), thiswep.m_id, WEP_CVAR_PRI(WEP_RIFLEGUN, tracer), WEP_CVAR_PRI(WEP_RIFLEGUN, shots), SND_RIFLEGUN_FIRE_PRIMARY, actor);
}

void W_Riflegun_Attack2(Weapon thiswep, entity actor, .entity weaponentity)
{
	W_Riflegun_FireBullet(thiswep, weaponentity, WEP_CVAR_SEC(WEP_RIFLEGUN, spread), WEP_CVAR_SEC(WEP_RIFLEGUN, damage), WEP_CVAR_SEC(WEP_RIFLEGUN, headshot_multiplier), WEP_CVAR_SEC(WEP_RIFLEGUN, force), WEP_CVAR_SEC(WEP_RIFLEGUN, solidpenetration), WEP_CVAR_SEC(WEP_RIFLEGUN, ammo), thiswep.m_id | HITTYPE_SECONDARY, WEP_CVAR_SEC(WEP_RIFLEGUN, tracer), WEP_CVAR_SEC(WEP_RIFLEGUN, shots), SND_RIFLEGUN_FIRE_SECONDARY, actor);
}

.void(Weapon thiswep, entity actor, .entity weaponentity) riflegun_bullethail_attackfunc;
.WFRAME riflegun_bullethail_frame;
.float riflegun_bullethail_animtime;
.float riflegun_bullethail_refire;
void W_Riflegun_BulletHail_Continue(Weapon thiswep, entity actor, .entity weaponentity, int fire)
{
	float r, af;

	Weapon sw = actor.(weaponentity).m_switchweapon; // make it not detect weapon changes as reason to abort firing
	af = ATTACK_FINISHED(actor, weaponentity);
	actor.(weaponentity).m_switchweapon = actor.(weaponentity).m_weapon;
	ATTACK_FINISHED(actor, weaponentity) = time;
	r = weapon_prepareattack(thiswep, actor, weaponentity, actor.riflegun_bullethail_frame == WFRAME_FIRE2, actor.riflegun_bullethail_refire);
	if(actor.(weaponentity).m_switchweapon == actor.(weaponentity).m_weapon)
		actor.(weaponentity).m_switchweapon = sw;
	if(r)
	{
		actor.riflegun_bullethail_attackfunc(thiswep, actor, weaponentity);
		weapon_thinkf(actor, weaponentity, actor.riflegun_bullethail_frame, actor.riflegun_bullethail_animtime, W_Riflegun_BulletHail_Continue);
	}
	else
	{
		ATTACK_FINISHED(actor, weaponentity) = af; // reset attack_finished if we didn't fire, so the last shot enforces the refire time
	}
}

void W_Riflegun_BulletHail(Weapon thiswep, entity actor, .entity weaponentity, float mode, void(Weapon thiswep, entity actor, .entity weaponentity) AttackFunc, WFRAME fr, float animtime, float refire)
{
	// if we get here, we have at least one bullet to fire
	AttackFunc(thiswep, actor, weaponentity);
	if(mode)
	{
		// continue hail
		actor.riflegun_bullethail_attackfunc = AttackFunc;
		actor.riflegun_bullethail_frame = fr;
		actor.riflegun_bullethail_animtime = animtime;
		actor.riflegun_bullethail_refire = refire;
		weapon_thinkf(actor, weaponentity, fr, animtime, W_Riflegun_BulletHail_Continue);
	}
	else
	{
		// just one shot
		weapon_thinkf(actor, weaponentity, fr, animtime, w_ready);
	}
}

.float bot_secondary_riflegunmooth;

METHOD(Riflegun, wr_aim, void(entity thiswep, entity actor, .entity weaponentity))
{
    PHYS_INPUT_BUTTON_ATCK(actor) = false;
    PHYS_INPUT_BUTTON_ATCK2(actor) = false;
    if(vdist(actor.origin - actor.enemy.origin, >, 1000))
        actor.bot_secondary_riflegunmooth = 0;
    if(actor.bot_secondary_riflegunmooth == 0)
    {
        if(bot_aim(actor, weaponentity, 1000000, 0, 0.001, false, true))
        {
            PHYS_INPUT_BUTTON_ATCK(actor) = true;
            if(random() < 0.01) actor.bot_secondary_riflegunmooth = 1;
        }
    }
    else
    {
        if(bot_aim(actor, weaponentity, 1000000, 0, 0.001, false, true))
        {
            PHYS_INPUT_BUTTON_ATCK2(actor) = true;
            if(random() < 0.03) actor.bot_secondary_riflegunmooth = 0;
        }
    }
}
METHOD(Riflegun, wr_think, void(entity thiswep, entity actor, .entity weaponentity, int fire))
{
    if(autocvar_g_balance_riflegun_reload_ammo && actor.(weaponentity).clip_load < min(WEP_CVAR_PRI(WEP_RIFLEGUN, ammo), WEP_CVAR_SEC(WEP_RIFLEGUN, ammo))) { // forced reload
        thiswep.wr_reload(thiswep, actor, weaponentity);
    } else
    {
        actor.(weaponentity).riflegun_accumulator = bound(time - WEP_CVAR(WEP_RIFLEGUN, bursttime), actor.(weaponentity).riflegun_accumulator, time);
        if(fire & 1)
        if(weapon_prepareattack_check(thiswep, actor, weaponentity, false, WEP_CVAR_PRI(WEP_RIFLEGUN, refire)))
        if(time >= actor.(weaponentity).riflegun_accumulator + WEP_CVAR_PRI(WEP_RIFLEGUN, burstcost))
        {
            weapon_prepareattack_do(actor, weaponentity, false, WEP_CVAR_PRI(WEP_RIFLEGUN, refire));
            W_Riflegun_BulletHail(thiswep, actor, weaponentity, WEP_CVAR_PRI(WEP_RIFLEGUN, bullethail), W_Riflegun_Attack, WFRAME_FIRE1, WEP_CVAR_PRI(WEP_RIFLEGUN, animtime), WEP_CVAR_PRI(WEP_RIFLEGUN, refire));
            actor.(weaponentity).riflegun_accumulator += WEP_CVAR_PRI(WEP_RIFLEGUN, burstcost);
        }
        if(fire & 2)
        {
            if(WEP_CVAR(WEP_RIFLEGUN, secondary))
            {
                if(WEP_CVAR_SEC(WEP_RIFLEGUN, reload)) {
                    thiswep.wr_reload(thiswep, actor, weaponentity);
                } else
                {
                    if(weapon_prepareattack_check(thiswep, actor, weaponentity, true, WEP_CVAR_SEC(WEP_RIFLEGUN, refire)))
                    if(time >= actor.(weaponentity).riflegun_accumulator + WEP_CVAR_SEC(WEP_RIFLEGUN, burstcost))
                    {
                        weapon_prepareattack_do(actor, weaponentity, true, WEP_CVAR_SEC(WEP_RIFLEGUN, refire));
                        W_Riflegun_BulletHail(thiswep, actor, weaponentity, WEP_CVAR_SEC(WEP_RIFLEGUN, bullethail), W_Riflegun_Attack2, WFRAME_FIRE2, WEP_CVAR_SEC(WEP_RIFLEGUN, animtime), WEP_CVAR_PRI(WEP_RIFLEGUN, refire));
                        actor.(weaponentity).riflegun_accumulator += WEP_CVAR_SEC(WEP_RIFLEGUN, burstcost);
                    }
                }
            }
        }
    }
}
METHOD(Riflegun, wr_checkammo1, bool(entity thiswep, entity actor, .entity weaponentity))
{
    float ammo_amount = GetResource(actor, thiswep.ammo_type) >= WEP_CVAR_PRI(WEP_RIFLEGUN, ammo);
    ammo_amount += actor.(weaponentity).(weapon_load[thiswep.m_id]) >= WEP_CVAR_PRI(WEP_RIFLEGUN, ammo);
    return ammo_amount;
}
METHOD(Riflegun, wr_checkammo2, bool(entity thiswep, entity actor, .entity weaponentity))
{
    float ammo_amount = GetResource(actor, thiswep.ammo_type) >= WEP_CVAR_SEC(WEP_RIFLEGUN, ammo);
    ammo_amount += actor.(weaponentity).(weapon_load[thiswep.m_id]) >= WEP_CVAR_SEC(WEP_RIFLEGUN, ammo);
    return ammo_amount;
}
METHOD(Riflegun, wr_resetplayer, void(entity thiswep, entity actor))
{
    for(int slot = 0; slot < MAX_WEAPONSLOTS; ++slot)
    {
        .entity weaponentity = weaponentities[slot];
        actor.(weaponentity).riflegun_accumulator = time - WEP_CVAR(WEP_RIFLEGUN, bursttime);
    }
}
METHOD(Riflegun, wr_reload, void(entity thiswep, entity actor, .entity weaponentity))
{
    W_Reload(actor, weaponentity, min(WEP_CVAR_PRI(WEP_RIFLEGUN, ammo), WEP_CVAR_SEC(WEP_RIFLEGUN, ammo)), SND_RELOAD);
}
METHOD(Riflegun, wr_suicidemessage, Notification(entity thiswep))
{
    return WEAPON_THINKING_WITH_PORTALS;
}
METHOD(Riflegun, wr_killmessage, Notification(entity thiswep))
{
    if(w_deathtype & HITTYPE_SECONDARY)
    {
        if(w_deathtype & HITTYPE_BOUNCE)
            return WEAPON_RIFLE_MURDER_HAIL_PIERCING;
        else
            return WEAPON_RIFLE_MURDER_HAIL;
    }
    else
    {
        if(w_deathtype & HITTYPE_BOUNCE)
            return WEAPON_RIFLE_MURDER_PIERCING;
        else
            return WEAPON_RIFLE_MURDER;
    }
}
METHOD(Riflegun, wr_zoom, bool(entity thiswep, entity actor))
{
    return PHYS_INPUT_BUTTON_ATCK2(actor) && WEP_CVAR(WEP_RIFLEGUN, secondary) == 0;
}

#endif
#ifdef CSQC

METHOD(Riflegun, wr_impacteffect, void(entity thiswep, entity actor))
{
    vector org2;
    org2 = w_org + w_backoff * 2;
    pointparticles(EFFECT_RIFLE_IMPACT, org2, w_backoff * 1000, 1);
    if(!w_issilent)
    {
        sound(actor, CH_SHOTS, SND_RIC_RANDOM(), VOL_BASE, ATTN_NORM);
    }
}
METHOD(Riflegun, wr_init, void(entity thiswep))
{
    if(autocvar_cl_reticle && autocvar_cl_reticle_weapon)
    {
        precache_pic("gfx/reticle_nex");
    }
}
METHOD(Riflegun, wr_zoom, bool(entity thiswep, entity actor))
{
    if(button_zoom || zoomscript_caught)
    {
        return true;
    }
    else
    {
        // no weapon specific image for this weapon
        return false;
    }
}
METHOD(Riflegun, wr_zoomdir, bool(entity thiswep))
{
    return button_attack2 && !WEP_CVAR(WEP_RIFLEGUN, secondary);
}

#endif
