#include "jeff.qc"
#include "colors.qc"

#include "crates.qh"
#include "explosives.qh"

#ifdef SVQC
	#include "randomweapons.qc"
	#include "crates.qc"
#endif

#include "huntingrifle.qc"
#include "explosives.qc"
#include "heli.qc"
#include "tankll48.qc"

#define IMPLEMENTATION
#include "vehicles/all.inc"
#include "heli.qc"
#include "tankll48.qc"
#undef IMPLEMENTATION
