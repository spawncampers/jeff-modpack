# Jeff's mods

This is game code for Xonotic servers:
- Jeff & Breakfast Vehicle Warfare;
- Jeff & Julius Resurrection Server;
- Jeff's Votable Server.

Visit our websites at:
- [https://jeffs.eu/](https://jeffs.eu/)

## Info

This mod is based on [SMB Modpack](https://github.com/MarioSMB/modpack).

Changes on top of SMB modpack:

- Added vehicle R22 Helicopter
- Added vehicle LL48 Tank
- Added crates mod
- Added blinking name mod
- Added new weapon: Hunting Rifle
- Added new weapon: Assault Rifle
- Added new weapon: Explosives
- New Announcer sounds
- Added random weapon placement
- Disabled all minigames
- Enabled all monsters
- Disabled model planter
- Enabled vehicle propeller damage
- Disabled classic SMB powerups

## Building the modpack:
To build the modpack you need to have jeff's xonotic-data fork placed into the modpack directory along with the QC compiler


Jeff's xonotic-data fork:
```bash
git clone https://gitlab.com/spawncampers/xonotic-data.pk3dir.git/
cd xonotic-data.pk3dir && git checkout darkblue/jeff
```

QC compiler 'gmqcc':
```bash
git clone https://gitlab.com/xonotic/gmqcc.git
cd gmqcc && make
```

Building the modpack:
```bash
./build.sh
```

## Installation

1. Copy the compiled `*.dat` files from `jeff-modpack/compiled` to `~/.xonotic/data` directory;
2. Copy `*.pk3` content packs from `content-packs` directory to `~/.xonotic/data` and make sure they're available on your map download server.
3. Download http://dl.xonotic.fps.gratis/zzz-misc-v006-110-ge06fbb8.pk3 to `~/.xonotic/data`
4. Download http://dl.xonotic.fps.gratis/zzz-quickmenu_029.pk3 to `~/.xonotic/data`

## License

This project is licensed under GNU General Public License, version 2 or later (GPLv2+).
